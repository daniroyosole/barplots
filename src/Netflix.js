import React, { useEffect, useState, useCallback } from 'react';
import jsonData from './netflix.json';

import './index.css';

const MAX_VALUE = 19;
const START_DATE = '2017-01-01';
const END_DATE = '2020-01-18';

const Netflix = () => {
    const [ranking, setRanking] = useState(null);
    const [cast, setCast] = useState(null);
    const [currentDate, setCurrentDate] = useState(null);
    const [sortedRanking, setSortedRanking] = useState([]);
    const [fullCast, setFullCast] = useState(null);

    useEffect(() => {
        const rawData = jsonData
            .filter(d => d.cast_arr.length &&
                !!d.date_added_f &&
                d.type === 'Movie' &&
                d.country &&
                d.country.indexOf('United States') > -1);

        let fullCastAux = [];
        const rawCast = {};
        rawData.forEach((d) => {
            const dateArr = rawCast[d.date_added_f] || [];
            rawCast[d.date_added_f] = [...dateArr, ...d.cast_arr];
            fullCastAux = [ ...fullCastAux, ...d.cast_arr ];
        });
        setFullCast(fullCastAux.filter((d, i) => fullCastAux.indexOf(d) === i));
        setCast(rawCast);
    }, [jsonData]);

    const makeRanking = useCallback((date, rankingObject) => {
        if (!cast[date]) return rankingObject;
        const newRankingObject = cast[date].reduce((r, d) => ({ ...r, [d]: (r[d] ? r[d] + 1 : 1) }), rankingObject);
        return newRankingObject;
    }, [cast]);

    useEffect(() => {
        if (cast) {
            const rankingDates = Object.keys(cast).filter(d => new Date(d) <= new Date(START_DATE));
            let newRanking = {};
            fullCast.forEach((d) => {
                newRanking[d] = 0;
            });
            rankingDates.forEach((date) => {
                newRanking = makeRanking(date, newRanking);
            });
            setRanking(newRanking);
            setCurrentDate(START_DATE);
        }
    }, [cast]);

    useEffect(() => {
        if (currentDate && new Date(currentDate) <= new Date(END_DATE)) {
            setTimeout(() => {
                const newDate = new Date(new Date(currentDate).getTime() + (3600000 * 24)).toISOString().substring(0, 10);
                setRanking(makeRanking(newDate, ranking));
                setCurrentDate(newDate);
            }, 100);
        }
    }, [currentDate]);

    useEffect(() => {
        if (ranking && ranking) {
            const rankingArr = Object.keys(ranking).map(d => ({ name: d, count: ranking[d] }));
            const order = {};
            JSON.parse(JSON.stringify(rankingArr)).sort((a, b) => b.count - a.count || a.name.localeCompare(b.name)).forEach((e, i) => {
                order[e.name] = i;
            });
            setSortedRanking(rankingArr.map(e => ({ ...e, order: order[e.name] })));
        }
    }, [ranking]);

    return (
        <div className="netflix">
            <p className="date-item">Date: {currentDate}</p>
            { sortedRanking.map(d => d.order < 10 ? (
                <p className={`item-row item-row-${d.order}`} style={{ top: `${d.order * 10}%`, width: `${d.count / MAX_VALUE * 100}%` }} key={d.name}>{d.name}: {d.count}</p>
            ) : (
                <p className="item-row" style={{ top: '120%', width: `${d.count / MAX_VALUE * 100}%` }} key={d.name}>{d.name}: {d.count}</p>
            )) }
        </div>
    )
}

export default Netflix;